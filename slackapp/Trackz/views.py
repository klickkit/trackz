from django.shortcuts import render
from Trackz.models import SlackUser, SlackRequest, Session, Absence, Task
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from django.utils import timezone


list_of_prescibed_operations = {}
username = ''
command = ''
list_of_responses = []

#--------------Utility functions begin ------------------#
def _days_hours_minutes(td):
    return td.days, td.seconds //3600, (td.seconds // 60) % 60


def _append(string):
    year_prefix = "20"  
    if string[0:2] != year_prefix:
        string = year_prefix + string
    return string


def _addup_queryset(sessions):
    days = 0
    seconds = 0
    mseconds = 0
    for session in sessions:
        duration = session.end_time - session.start_time
        days = days + duration.days
        seconds = seconds + duration.seconds
        mseconds = mseconds + duration.microseconds
    resulting_timedelta = timedelta(days, seconds, mseconds)
    return resulting_timedelta


def _check_for_task_object(task):
    corresponding_object = Task.objects.get(name=task)
    return corresponding_object

def _extract_user(username):
    user = SlackUser.objects.get(user__username=username)
    return user


def _session_task(session, task, name):
    task.name = name
    task.save()
    session.task = task
    session.save()


def _extract_text(text):
    temp = []
    string = ""
    index = 0
    for i in text:
        if i != '/' and i != '-' and i != '$':
            string = string + i
        else:                                                   #Format : "/presence? 12/8/16-13/8/16 $username", "/absence 12/8/16"
            if i == '-':
                string = _append(string)
            elif i == '$':
                index = text.index('$')
                break
            temp._append(int(string))
            string = ""
    string = _append(string)
    string = string.rstrip()
    temp._append(int(string))
    user = text[index + 1:]
    return temp, user
#----------------Utility functions end ---------------Request functions start

def _get_status(request, username, interval): # 
    response = ""
    slack_user = _extract_user(username)
    interval = int(interval)
    tasks = Task.objects.filter(user=slack_user)
    if tasks is None:
        response = "The user has no associated tasks"
    for index, i in enumerate(tasks):
        sessions = Session.objects.filter(user=slack_user, 
        start_time__gte = timezone.now() - timedelta(days=interval), task=i)
        if sessions is not None:
            resulting_timedelta = _addup_queryset(sessions)
            days, hours, minutes = _days_hours_minutes(resulting_timedelta)
            response__append = "on {}. ".format(i)
            response = response + """You've worked for {} days, {} hours and 
            {} minutes """.format(days, hours, minutes) + response__append +"\n"
        else:
            continue 
    if not response:
        response = "You have not taken up any work in the specified time period" 
    return HttpResponse(response)


def _presence(request, username, text): #Format Date 
    date_list, user = _extract_text(text)
    date = datetime(date_list[2], date_list[1], date_list[0])
    if user is None:
        slack_user = SlackUser.objects.get(user__username=username)
    slack_user = SlackUser.objects.get(user__username=user)
    try:
        Absence.objects.get(start_date__lte=date, user=slack_user, 
        end_date__gte=date, presence=False)
    except ObjectDoesNotExist:
        absence_entry = slack_user.absence_set.create(start_date=date, 
        end_date=date, presence=False)
        exist_boolean = Session.objects.filter(start_time__date__lte=date, 
        end_time__date__gte=date, user =slack_user).exists() #absence_flag is false, absence_flag can be true or it can be null
        if exist_boolean == True:
            absence_entry.presence = True
            absence_entry.save()
            response = "{} was present on the date {}".format(slack_user.user.username, date)
            return response
        absence_entry.save()
    response = "{} was not present on the  date {}".format(slack_user.user.username, date)
    return response 


def _login(request, username, text):
    slack_user = _extract_user(username)
    string = ''
    office_presence = 'WFH'
    task = ''
    if text:
        if text[:3] == 'WFH':
            task = text[3:].lstrip()
        elif text[len(text) - 3:] == 'WFH':
            task = text[:len(string) - 3].rstrip() 
        else:
            office_presence = 'WFO'
            task = text  
    
    last_session = Session.objects.filter(user=slack_user).last()
    new_task = Task(user=slack_user)
    new_session = slack_user.session_set.create(office_presence=office_presence)
    print last_session is None, last_session
    if last_session is None or last_session.task.is_complete:
        if not task:
            response = "No pending tasks. Take up a new task"
            return response
        else:
            try:
                corresponding_object = _check_for_task_object(task)
                new_session.task = corresponding_object
                new_session.save()
            except ObjectDoesNotExist:
                _session_task(new_session, new_task, task)
    
    if last_session is not None and not last_session.task.is_complete:
        if task:
            _logout(request, username)
            try:
                corresponding_object = _check_for_task_object(task)
                new_session.task = corresponding_object
                new_session.save()
            except ObjectDoesNotExist:
                _session_task(new_session, new_task, task)
        else:
            new_session.task = last_session.task
            new_session.save()
    response = "Session started You've started working on {}".format(new_session.task)
    return response
        
    
def _absence(request, username, text):
    slack_user = SlackUser.objects.get(user__username=username)
    text = text.lstrip()
    date_list = []
    date_list, user = _extract_text(text)
    date1 = datetime(date_list[2], date_list[1], date_list[0])
    if len(date_list) == 6:
        date2 = datetime(date_list[5], date_list[4], date_list[3]) 
        absence = slack_user.absence_set.create(start_date=date1, 
        end_date=date2, presence=False)
    else:
        absence = slack_user.absence_set.create(start_date=date1, 
        end_date=date1, presence=False)
    absence.save()      
    slack_user = absence.user
    username = slack_user.user.username
    response = " Hey {} Your leave request has been successfully recorded".format(username)
    return response

def _break_utility(username, text): 
    session = Session.objects.filter(user__user__username=username).last()
    if session is None or session.end_time is not None:
        return HttpResponse("Logout or break request without a matching login", 
        status=404) 
    session.break_reason = text
    session.end_time = timezone.now()
    session.save()
    return session


def _logout(request, username):
    text = "Completion"
    session = _break_utility(username, text)
    if type(session) is Session:
        task = Task.objects.get(name=session.task.name)
        task.is_complete = True
        task.save()
        sessions_for_a_specific_task = Session.objects.filter(task=session.task)
        resulting_timedelta = _addup_queryset(sessions_for_a_specific_task)
        days, hours, minutes = _days_hours_minutes(resulting_timedelta)
        response__append = "on {}. ".format(task)
        response = """Congrats on completing your work,
        You've worked for {} days, 
        {} hours and {} minutes """.format(days, hours, minutes) + response__append + "Well done!!"
    else:
        response = session
    return response


def _take_break(request, username, text):
    completed_session = _break_utility(username, text)
    task = completed_session.task
    timedelta = completed_session.end_time - completed_session.start_time
    days, hours, minutes = _days_hours_minutes(timedelta)
    response__append = "on {}. ".format(task)
    response = "You've worked for {} days, {} hours and {} minutes ".format(days, hours, minutes) + response__append + "Well done!!"
    return "You have taken a break for {}, See you . ".format(text) + response
    
def handle_request(request):
    if request.method == 'GET':
        return render(request, 'Trackz/request.html')
    if request.method == 'POST':
        username = request.POST['user_name']
        command = request.POST['command']
        team_id = request.POST['team_id']
        text = request.POST['text']
        user, created = User.objects.get_or_create(username=username)
        slack_user, created = SlackUser.objects.get_or_create(user=user, 
        defaults={'team_id':team_id})
        if created and command == '/logout':
            return HttpResponse("""Hey! This is your first request and why do 
                you want to start it with a logout""" , 
                status=404)
        slack_request = slack_user.slackrequest_set.create(command=command)
        slack_request.save()
        list_of_prescibed_operations = {
            '/login' : _login, 
            '/logout' : _logout, 
            '/status' : _get_status, 
            '/absence' : _absence , 
            '/presence?' : _presence, 
            '/break' : _take_break
        }
        if command in list_of_prescibed_operations.keys():
            if command == '/logout':
                response = _logout(request, username)
            else:
                response = list_of_prescibed_operations[command](request, username, text)
        return HttpResponse(response)
