from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from django.utils import timezone


class SlackUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    team_id = models.CharField(max_length=100, blank=True, default='t001')
    

class SlackRequest(models.Model):
    user = models.ForeignKey(SlackUser)
    command = models.CharField(max_length=10, blank=True, default='/login')

    def __str__(self):
        return self.user.username


class Task(models.Model):
    name = models.CharField(max_length=100, blank=True, default=None, unique=True)
    user = models.ForeignKey(SlackUser, on_delete=models.CASCADE)
    is_complete = models.BooleanField(default=False)

    def __str__(self):
        return self.name
        

class Session(models.Model):
    user = models.ForeignKey(SlackUser)
    start_time = models.DateTimeField(auto_now_add=True)
    end_time = models.DateTimeField(null=True, default=None)
    office_presence = models.CharField(max_length=10, default='WFO')
    break_reason = models.CharField(max_length=100, default=None, null=True)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, default=None, null=True)

    def __str__(self):
        return self.user.user.username

    
class Absence(models.Model):        
    user = models.ForeignKey(SlackUser, on_delete=models.CASCADE, default=None)
    start_date = models.DateField(default=datetime.now().date())
    end_date = models.DateField(default=start_date)
    presence = models.BooleanField(default=None)

    class Meta:
        unique_together = ('user', 'start_date')














    

    
